package com.batch208.postApp.models;

import javax.persistence.*;
import java.util.Set;

// Mark this Java Object as a representation of a database table via the use of @Entity
@Entity
// Designate the table name that this model is connected to:
@Table(name="posts")
public class Post {

    // Indicate that the following property is a primary key via @Id
    @Id
    // Auto-increment the id property using @GenerateValue
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // Class properties that represent table columns in a relational database as @Column
    @Column
    private String title;

    @Column
    private String content;

    @ManyToOne
    @JoinColumn(name="user_id",nullable = false)
    private User user;
    // We represent and tell Hibernate that this class represents the many side of relationship.
    // And that each post is linked to a user via the joincolumn, a reference using a foreign key
    // to a user primary key.

    // This column is also mapped by our user, that when the getPost() method of the User is called,
    // all post rows, data belonging to the user will be gathered and returned.

    // user_id will be the name of the foreign_key column.

      //////////////////////////////////////
     // Constructors and Setters/Getters //
    // ///////////////////////////////////

    // default constructor needed when retrieving posts
    public Post(){}

    //parameterized constructor needed when creating posts
    public Post(String title, String content){
        this.title = title;
        this.content = content;
    }

    public String getTitle(){
        return title;
    }

    public void setTitle(String title){
        this.title = title;
    }

    public String getContent(){
        return content;
    }

    public void setContent(String content){
        this.content = content;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
