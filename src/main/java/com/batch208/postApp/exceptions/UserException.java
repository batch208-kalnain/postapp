package com.batch208.postApp.exceptions;

public class UserException extends Exception{

    public UserException(String message){
        super(message);
    }

}
