package com.batch208.postApp.repositories;

import com.batch208.postApp.models.Post;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

// An interface contains behaviors or methods that a class should contain.

// An interface marked as @repository means that the interface would contain or contains
// methods for database manipulation
@Repository
public interface PostRepository extends CrudRepository<Post,Object> {
// By extending CrudRepository, PostRepository has inherited pre-defined methods
// for creating, retrieving, updating and deleting records.


}
