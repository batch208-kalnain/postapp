package com.batch208.postApp.controllers;

import com.batch208.postApp.models.Post;
import com.batch208.postApp.services.PostService;
import org.apache.coyote.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

// Handle all HTTP responses
@RestController
// Enables cross origin resource requests
// CORS - cross origin resource sharing is the ability to allow or disallow applications to share
// resources from each other. With this, we can allow or disallow clients to access our API.
@CrossOrigin
public class PostController {

    @Autowired
    PostService postService;

    //map web request to controller methods via @RequestMapping
    //@RequestMapping(value="/posts",method= RequestMethod.POST)
    @PostMapping("/posts")

    // ResponseEntity is an object that represents and contains the whole HTTP response,
    // the actual message, the status code.
    // @RequestBody automatically converts JSON client input into our desired object.
    public ResponseEntity<Object> createPost(@RequestHeader(value="Authorization") String stringToken,@RequestBody Post post){
    // @RequestHeader(value="Authorization") allow us to get the value passed as Authorization from our request.

        //public ResponseEntity<Object> createPost(@RequestBody Post post){
        // @RequestBody converts the JSON input as the desired class of the parameter.
        // System.out.println(post.getTitle());
        // System.out.println(post.getContent());

        // Use the createPost() method from our service and pass our post object.
        //postService.createPost(post);
        postService.createPost(stringToken,post);
        /*
        *  RequestBody(JSON) -> Post post object in the controller -> postService createPost(post)
        *  -> postRepository.save(post)
        *
        */

        return new ResponseEntity<>("Post Created Successfully",HttpStatus.CREATED);
    }

    @GetMapping("/posts")
    public ResponseEntity<Object>getPosts(){
        return new ResponseEntity<>(postService.getPosts(),HttpStatus.OK);
    }

    @PutMapping("/posts/{id}")
    public ResponseEntity<Object> updatePost(@RequestHeader(value="Authorization")String stringToken, @PathVariable Long id,@RequestBody Post post){
        //System.out.println(id);
        //System.out.println(post.getTitle());
        //System.out.println(post.getContent());

        // pass the id and the request body as post to our postService method.
        // As the return method is ResponseEntity already, we no longer need to return as ResponseEntity
        // here in our controller.
        return postService.updatePost(stringToken,id,post);
    }

    @DeleteMapping("/posts/{id}")
    public ResponseEntity<Object> deletePost(@RequestHeader(value="Authorization")String stringToken, @PathVariable Long id){
        return postService.deletePost(stringToken,id);
        //return new ResponseEntity<>("Post deleted successfully",HttpStatus.OK);
    }

}
