package com.batch208.postApp.controllers;

import com.batch208.postApp.exceptions.UserException;
import com.batch208.postApp.models.User;
import com.batch208.postApp.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@CrossOrigin
public class UserController {

    @Autowired
    UserService userService;

    @PostMapping("/users/register")
    public ResponseEntity<Object> register(@RequestBody Map<String,String> body)throws UserException {
        String username = body.get("username");

        if(!userService.findByUsername(username).isEmpty()){
            throw new UserException("Username already exist");
        }else{
            String password = body.get("password");
            String encodedPassword = new BCryptPasswordEncoder().encode(password);

            User newUser = new User(username,encodedPassword);
            userService.createUser(newUser);

            return new ResponseEntity<>("User registered successfully",HttpStatus.CREATED);
        }
        //userService.createUser(user);
        //return new ResponseEntity<>("User has been created successfully", HttpStatus.OK);
    }

    @GetMapping("/users")
    public ResponseEntity<Object> getUsers(){
        return new ResponseEntity<>(userService.getUsers(),HttpStatus.OK);
    }

    @PutMapping("users/{id}")
    public ResponseEntity<Object> updateUser(@PathVariable Long id,@RequestBody User user){
        userService.updateUser(id,user);
        return new ResponseEntity<>("User has been updated successfully",HttpStatus.OK);
    }

    @DeleteMapping("users/{id}")
    public ResponseEntity<Object> deleteUser(@PathVariable Long id){
        userService.deleteUser(id);
        return new ResponseEntity<>("User has been deleted successfully",HttpStatus.OK);
    }

}
