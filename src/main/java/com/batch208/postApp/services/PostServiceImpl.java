package com.batch208.postApp.services;

import com.batch208.postApp.config.JwtToken;
import com.batch208.postApp.models.Post;
import com.batch208.postApp.models.User;
import com.batch208.postApp.repositories.PostRepository;
import com.batch208.postApp.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

// @Service is added so that springboot would recognize that business logic is
// implemented in this layer.
@Service
public class PostServiceImpl implements PostService{

    // We're going to create an instance of the postRepository that is persistent
    // within our springboot app.
    // This is so we can use the pre-defined methods for database manipulation for our table.
    @Autowired
    private PostRepository postRepository;

    @Autowired
    JwtToken jwtToken;

    @Autowired
    private UserRepository userRepository;

    public void createPost(String stringToken, Post post){
        // Get the username from the token using jwtToken getUserNameFromToken method.
        // Then, pass the username:
        // Get the user's details from our table, using oyr userRepository findByUsername
        User author = userRepository.findByUsername(jwtToken.getUsernameFromToken(stringToken));
        // Check if you can get the user from the table.
        //System.out.println(author.getUsername());

        Post newPost = new Post();
        newPost.setTitle(post.getTitle());
        newPost.setContent(post.getContent());
        newPost.setUser(author);

        postRepository.save(newPost);
        // When createPost is used from our services, we will be able to pass a post
        // class object and then, using the .save() method of our postRepository we
        // will be able to insert a new row into our posts table.

        //postRepository.save(post);
    }

    public Iterable<Post> getPosts(){
        return postRepository.findAll();
    }

    public ResponseEntity updatePost(String stringToken,Long id,Post post){
        // findById() retrieves a record that matches the passed id

        // post paremeter here is actually the request object passed from the controller.
        // because post parameter is actually the request body converted as a post
        // class object, it has the methods from a post class object.
        // Therefore, getTitle() will actually get the value of the title property from
        // the request body.

        /*System.out.println("This is the id passed as path variable");
        System.out.println(id);*/

        Post postForUpdating = postRepository.findById(id).get();

        // After getting the user of the post, get the username of the user.
        String postAuthor = postForUpdating.getUser().getUsername();

        String authenticatedUser =  jwtToken.getUsernameFromToken(stringToken);
        //System.out.println(authenticatedUser.equals(postAuthor));

        //check if the user trying to update is the same as the author.
        if(authenticatedUser.equals(postAuthor)){
            // The found post title will be updated with the title of the request body as post
            postForUpdating.setTitle(post.getTitle());
            // The found post content will be updated with the content of the request body as post
            postForUpdating.setContent(post.getContent());
            // save the updates to our data
            postRepository.save(postForUpdating);

            /*System.out.println("This is the found data using the id");
            System.out.println(postForUpdating.getTitle());

            System.out.println("This is the request body passed from the controller");
            System.out.println(post.getTitle());*/

            return new ResponseEntity<>("Post Updated Successfully", HttpStatus.OK);
        }else{
            return new ResponseEntity<>("You are not authorized to update this post", HttpStatus.UNAUTHORIZED);
        }

    }


    public ResponseEntity deletePost(String stringToken, Long id){
        Post postToBeDeleted = postRepository.findById(id).get();
        String postAuthor = postToBeDeleted.getUser().getUsername();
        String authenticatedUser =  jwtToken.getUsernameFromToken(stringToken);

        if(authenticatedUser.equals(postAuthor)){
            // Delete the record that matches the passed id
            postRepository.deleteById(id);

            return new ResponseEntity<>("Post Deleted Successfully",HttpStatus.OK);
        }else{
            return new ResponseEntity<>("You are not authorized to update this post",HttpStatus.UNAUTHORIZED);
        }
    }

}
