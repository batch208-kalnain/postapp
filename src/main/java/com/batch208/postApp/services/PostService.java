package com.batch208.postApp.services;

import com.batch208.postApp.models.Post;
import org.apache.coyote.Response;
import org.springframework.http.ResponseEntity;

public interface PostService {

    // Methods to manipulate post table
    void createPost(String stringToken,Post post);

    Iterable<Post>getPosts();

    ResponseEntity updatePost(String stringToken, Long id, Post post);

    ResponseEntity deletePost(String stringToken,Long id);

}
